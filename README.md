# airFrance project

    ============= Serigne FALL =======================

## Description

A Springboot API that exposes two services:

    • one that allows to register a user

    • one that displays the details of a registered user

##IDE
    . IntelliJ IDEA 

## Installation
    . Lombok   
    . git version 2.20.1
    . Maven version 3.6.0    

## technologies and dependencies

✓ Java version: 8

✓ Packaging: Jar

✓ Dependencies

    • swagger

    • Spring Data JPA

    • Spring Web

    • H2 Database

    • Lombok

    . MapStruct

##Configuration H2 database

   > Saved Settings: Generic H2 (Embedded)

   > Setting Name: Generic H2 (Embedded)
 
   > Drive class : org.h2.Driver

   > JDBC URL : jdbc:h2:mem:airfrancedb
 
   > User Name: sa
 
   > password:
    

##Test Integrated H2 database

    .  http://localhost:8080/h2-console, this is the link that allows us to check the data stored in the database on the browser.###

## Postman collection
    =========== creation Status CREATED ======================
    { 
        "name":"Serigne FALL",

        "birthDate":"1986-12-06",

        "countryId":1,

        "phoneNumber":"+221 77 329 70",

        "gender":"MALE"

    }

=========== creation Status NOT ACCEPTED ======================

    ------------>> country is not frensh

    {
        "name":"Serigne FALL",
    
        "birthDate":"1986-12-06",

        "countryId":3,

        "phoneNumber":"+221 77 329 70",

        "gender":"MALE"

    }

    ------------>> years < 18

    {
        "name":"Serigne FALL",
    
        "birthDate":"2020-12-06",

        "countryId":1,

        "phoneNumber":"+221 77 329 70",

        "gender":"MALE"

    }
=========== creation Status NOT BAD_REQUEST ======================

    {
        "name":null,
    
        "birthDate":"2020-12-06",

        "countryId":1,

        "phoneNumber":"+221 77 329 70",

        "gender":"MALE"

    }
##link Swagger

    . http://localhost:8080/swagger-ui.html 


##Tests Unitaires
    mvn clean test
