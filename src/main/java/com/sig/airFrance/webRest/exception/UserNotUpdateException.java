package com.sig.airFrance.webRest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserNotUpdateException extends RuntimeException  {


      public UserNotUpdateException(String sms) {
            super("user not update :  "+ sms);
        }

}
