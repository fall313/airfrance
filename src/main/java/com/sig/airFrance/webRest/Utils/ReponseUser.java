package com.sig.airFrance.webRest.Utils;

import com.sig.airFrance.service.dto.UserDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReponseUser {

    String message="";

    String status="NOK";

    UserDto userDto;
}
