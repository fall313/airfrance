package com.sig.airFrance.webRest;


import com.sig.airFrance.service.IUserService;
import com.sig.airFrance.service.dto.UserDto;

import com.sig.airFrance.service.utils.Constante;
import com.sig.airFrance.webRest.Utils.HeaderUtil;
import com.sig.airFrance.webRest.Utils.ReponseUser;
import com.sig.airFrance.webRest.exception.UserAcceptException;
import com.sig.airFrance.webRest.exception.UserNotCreateException;

import com.sig.airFrance.webRest.exception.UserNotFoundException;
import com.sig.airFrance.webRest.exception.UserNotUpdateException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.springframework.http.ResponseEntity;

/**
 * REST controller for managing User.
 */
@RestController
@Api(description = "API for CRUD  on users.")
@RequestMapping(Constante.API)
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private static final String ENTITY_NAME = "airFranceUserResource";

    private final IUserService userService;


    public UserResource(IUserService userService) {
        this.userService = userService;

    }

    /**
     * POST  /users : Create a new User.
     *
     * @param userDto the UserDto to create
     * @return the ResponseEntity with status 201 (Created) and with body the new UserDto, or with status 400 (Bad Request) if the User has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping(Constante.API_CREATE)
    @ApiOperation(value = Constante.INFO_API_CREATE)
    public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserDto userDto) throws URISyntaxException {

        log.debug("REST request to save User : {}", userDto);
        if (userDto.getId() != null) {
            throw new UserNotCreateException(Constante.SMS_ERRER_NOT_CREAT_ID_NOT_NULL);
        }
        log.debug("#---------------------->>> validation Dto ");
        ReponseUser result = userService.validation(userDto);
        if (result.getStatus().equals(Constante.CODE_REPOSE_NOK)) {
            throw new UserAcceptException(result.getMessage());
        }
        userDto = userService.save(userDto);

        return ResponseEntity.created(new URI("/api/users/" + userDto.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, userDto.getId().toString()))
                .body(userDto);

    }


    /**
     * GET  /users/:id : get the "id" user.
     *
     * @param id the id of the UserDto to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the UserDto, or with status 404 (Not Found)
     */

    @ApiOperation(value = Constante.INFO_API_GET)
    @GetMapping(Constante.API_GET)
    //  @Timed
    public ResponseEntity<Object> getUserById(@PathVariable Long id) {
        log.debug("REST request to get User : {}", id);

        UserDto userDto = userService.getById(id);
        if (userDto == null) {
            throw new UserNotFoundException(id);
        }
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    /**
     * DELETE/ delete the id user
     *
     * @param id
     * @return
     */
    @ApiOperation(value = Constante.INFO_API_DELETE)
    @DeleteMapping(Constante.API_DELETE)
    public ResponseEntity<Object> deletePromoteur(@PathVariable Long id) {
        log.debug("REST request to delete user : {}", id);

        try {
            userService.deleteUserById(id);
            return new ResponseEntity<>(Constante.SMS_USER_DELETE_SUCCESS, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(Constante.SMS_USER_NOT_FOUND, HttpStatus.NO_CONTENT);
        }

    }

    /**
     * PUT/ users Updates an existing user.
     *
     * @param userDto
     * @return
     * @throws URISyntaxException
     */

    @ApiOperation(value = Constante.INFO_API_UPDATE)
    @PutMapping(Constante.API_UPDATE)
    public ResponseEntity<UserDto> update(@Valid @RequestBody UserDto userDto) throws URISyntaxException {


        log.debug("REST request to update UserDto : {}", userDto);
        if (userDto.getId() == null) {
            throw new UserNotUpdateException(" Invalid id" + ENTITY_NAME + "idnull");
        }

        log.debug("#---------------------->>> validation Dto ");
        ReponseUser reponseUser = userService.validation(userDto);
        if (reponseUser.getStatus().equals(Constante.CODE_REPOSE_NOK)) {
            throw new UserAcceptException(reponseUser.getMessage());
        }

        UserDto result = userService.save(userDto);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);

    }

    /**
     * GET  /users : get all the users.
     *
     * @return
     */

    @ApiOperation(value = Constante.INFO_API_LIST)
    @GetMapping(value = Constante.API_LIST)
    public List<UserDto> getAllUser() {
        log.debug("REST request to get all User");
        return userService.getAllUser();
    }

}
