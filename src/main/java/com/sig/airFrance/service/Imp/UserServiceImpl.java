package com.sig.airFrance.service.Imp;

import com.sig.airFrance.domain.CountryEntity;
import com.sig.airFrance.domain.UserEntity;
import com.sig.airFrance.repository.CountryRepository;
import com.sig.airFrance.repository.UserRepository;
import com.sig.airFrance.service.IUserService;
import com.sig.airFrance.service.dto.UserDto;
import com.sig.airFrance.service.mapper.UserMapper;
import com.sig.airFrance.service.utils.AirFranceUtils;
import com.sig.airFrance.service.utils.Constante;
import com.sig.airFrance.webRest.Utils.ReponseUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class UserServiceImpl implements IUserService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;

    private final CountryRepository countryRepository;

    private final UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository, CountryRepository countryRepository, UserMapper userMapper) {

        this.userRepository = userRepository;
        this.countryRepository = countryRepository;
        this.userMapper = userMapper;
    }

    /**
     * save a new users
     *
     * @param userDto
     * @return
     */
    @Override
    public UserDto save(UserDto userDto) {
        log.debug("Request to save user : {}", userDto);
        UserEntity entity = userMapper.toEntity(userDto);
        entity = userRepository.save(entity);
        log.debug("===================  create user OK ============================");
        return userMapper.toDto(entity);
    }


    /**
     * find all user
     *
     * @return
     */
    @Override
    public List<UserDto> getAllUser() {
        return userRepository.findAll()
                .stream()
                .map(us -> userMapper.toDto(us))
                .collect(Collectors.toList());
    }

    /**
     * delete user by id
     *
     * @param id
     */
    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    /**
     * get user by id
     *
     * @param id
     * @return
     */
    @Override
    public UserDto getById(Long id) {

        log.debug("Request to get user : {}", id);
        UserEntity entity = userRepository.findById(id).orElse(null);
        return userMapper.toDto(entity);
    }


    /**
     * validation  (ages>=18 and country)
     *
     * @param userDto
     * @return
     */
    @Override
    public ReponseUser validation(UserDto userDto) {

        ReponseUser reponse = new ReponseUser();

        if (!AirFranceUtils.isAdult(userDto.getBirthDate())) {
            reponse.setMessage(Constante.SMS_ERRER_NOT_CREAT_COUNT_AGES);
            log.error("{} {}", reponse.getMessage(), userDto.getBirthDate());
            return reponse;
        }

        if (userDto.getCountryId() == null || userDto.getCountryId() <= 0) {
            reponse.setMessage(Constante.SMS_ERRER_NOT_CREAT_COUNT_COUNTRY_NULLID);
            log.error("{} {}", reponse.getMessage(), userDto.getCountryId());
            return reponse;
        }

        CountryEntity countryEntity = countryRepository.findById(userDto.getCountryId()).orElse(null);
        log.debug("get a country -> {}", countryEntity);

        if (countryEntity == null) {

            reponse.setMessage(Constante.SMS_ERRER_NOT_CREAT_COUNT_COUNTRY);
            log.error("{} {}", reponse.getMessage(), userDto.getCountryId());
            return reponse;
        }
        if (!AirFranceUtils.isFrance(countryEntity.getCode())) {

            reponse.setMessage(Constante.SMS_ERRER_NOT_CREAT_COUNT_COUNTRY);
            log.error("{} {}", reponse.getMessage(), countryEntity.getCode());
            return reponse;
        }

        reponse.setStatus(Constante.CODE_REPOSE_OK);
        return reponse;
    }
}
