package com.sig.airFrance.service.Imp;

import com.sig.airFrance.domain.CountryEntity;

import com.sig.airFrance.repository.CountryRepository;

import com.sig.airFrance.service.ICountryService;
import com.sig.airFrance.service.dto.CountryDto;

import com.sig.airFrance.service.mapper.CountryMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CountryServiceImp implements ICountryService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    private final CountryRepository countryRepository;

    private final CountryMapper countryMapper;



    public CountryServiceImp(CountryRepository countryRepository, CountryMapper countryMapper) {
        this.countryRepository = countryRepository;
        this.countryMapper = countryMapper;
    }


    @Override
    public CountryDto save(CountryDto countryDto) {
        log.debug("Request to save CountryDto : {}", countryDto);
        CountryEntity entity=countryMapper.toEntity( countryDto);
        entity=countryRepository.save(entity);
        return countryMapper.toDto(entity);
    }

    @Override
    public List<CountryEntity> getAll() {

        log.debug("Request to get all  country ");
        return countryRepository.findAll();



    }
}
