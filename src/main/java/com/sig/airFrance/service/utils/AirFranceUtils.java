package com.sig.airFrance.service.utils;


import java.time.LocalDate;
import java.time.Period;

public class AirFranceUtils {

    public static Integer calculAges(LocalDate birthDate){
        Period diff = Period.between(birthDate, LocalDate.now());
        return diff.getYears();
    }

    public static Boolean isAdult(LocalDate birthDate){
        return calculAges(birthDate)>=Constante.AGE_MIN;
    }

    public static Boolean isFrance(String codeContry){
        return codeContry.equals(Constante.CODE_PAYS_FR);
    }

}
