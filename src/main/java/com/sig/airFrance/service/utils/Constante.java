package com.sig.airFrance.service.utils;

public class Constante {

    public static final String CODE_PAYS_FR = "Fr";
    public static final String SMS_ERRER_NOT_CREAT_COUNT_COUNTRY = "You are not allowed to create an account because you're not French!)";
    public static final String SMS_ERRER_NOT_CREAT_COUNT_AGES = "You are not allowed to create an account because you are a minor";
    public static final String CODE_REPOSE_OK = "OK";
    public static final String CODE_REPOSE_SMS = "creation has been successfully ";
    public static final String CODE_REPOSE_NOK = "NOK";
    public static final String SMS_ERRER_NOT_CREAT_COUNT_COUNTRY_NULLID ="ID COUNTRY IS NULL" ;
    public static final String SMS_ERRER_NOT_CREAT_ID_NOT_NULL ="A new user cannot already have an ID" ;
    public static final String INFO_API_CREATE = "Create a new User";
    public static final String INFO_API_GET ="get the id user." ;
    public static final String SMS_USER_NOT_FOUND = "User not found";
    public static final String SMS_USER_NOT_CONTENT = "No Contentn user";
    public static final String SMS_VALIDE_BIRTHDATE = "birth date is required";
    public static final Object SMS_USER_CREAT_SECESS = "creation has been successfully ";



    public static final String INFO_API_DELETE = "delete the user by id";
    public static final String INFO_API_LIST = "liste user ";
    public static final String API_CREATE = "/users";
    public static final String API_DELETE = "/users/{id}";
    public static final String API_LIST = "/users";
    public static final String API_UPDATE = "/users";
    public static final String API_GET = "/users/{id}";
    public static final String API = "/api";
    public static final String INFO_API_UPDATE = "Updates an existing user.";
    public static final Object SMS_USER_DELETE_SUCCESS = "delete has been successfully";
    ;
    static final Integer AGE_MIN=18;
}
