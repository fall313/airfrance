package com.sig.airFrance.service.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.sig.airFrance.domain.Gender;
import com.sig.airFrance.service.utils.Constante;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserDto {
    private Long id;

    @NotNull
    @NotBlank(message = "name is mandatory")
    @ApiModelProperty(notes = "name user")
    private String name;
    
    @NotNull(message = Constante.SMS_VALIDE_BIRTHDATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
    @NotNull
    private Long countryId;

    private String phoneNumber;

	@Enumerated(EnumType.STRING)
	private Gender gender;



}
