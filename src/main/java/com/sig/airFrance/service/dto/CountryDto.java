package com.sig.airFrance.service.dto;


import com.sig.airFrance.domain.UserEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CountryDto implements Serializable {


    private long id;

    private String name;

    private String code;


}
