package com.sig.airFrance.service.mapper;



import com.sig.airFrance.domain.UserEntity;
import com.sig.airFrance.service.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Criticite and its DTO CriticiteDTO.
 */
@Mapper(componentModel = "spring", uses = {CountryMapper.class})
public interface UserMapper extends EntityMapper<UserDto, UserEntity> {


    @Mapping(source = "country.id", target = "countryId")
    UserDto  toDto(UserEntity userEntity);


    @Mapping(source = "countryId", target = "country")
    UserEntity toEntity(UserDto subscriptionDTO);

    default UserEntity fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        return userEntity;
    }
}
