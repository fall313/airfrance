package com.sig.airFrance.service.mapper;


import com.sig.airFrance.domain.CountryEntity;
import com.sig.airFrance.service.dto.CountryDto;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity CountryEntity and its DTO CountryDto.
 */
@Mapper(componentModel = "spring")
public interface CountryMapper extends EntityMapper<CountryDto, CountryEntity> {



    default CountryEntity fromId(Long id) {
        if (id == null) {
            return null;
        }
        CountryEntity country = new CountryEntity();
        country.setId(id);
        return country;
    }
}
