package com.sig.airFrance.service;

import com.sig.airFrance.domain.CountryEntity;
import com.sig.airFrance.service.dto.CountryDto;

import java.util.List;

public interface  ICountryService {


    CountryDto save(CountryDto countryDto);

    List<CountryEntity> getAll();
}
