package com.sig.airFrance.service;

import com.sig.airFrance.service.dto.UserDto;
import com.sig.airFrance.webRest.Utils.ReponseUser;

import java.util.List;

public interface IUserService {
    UserDto save(UserDto userDto);

    List<UserDto> getAllUser();

    void deleteUserById(Long id);

    UserDto getById(Long id);

    ReponseUser validation(UserDto userDto);
}
