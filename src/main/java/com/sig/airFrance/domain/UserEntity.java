package com.sig.airFrance.domain;



import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CacheConcurrencyStrategy;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@Entity
@Table(name = "user")
@EqualsAndHashCode
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class UserEntity implements Serializable {


    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private long id;

    /**
     * name user
     */
    @Column(name = "name", nullable = false,length = 100)
    @NotBlank(message = "name is mandatory")
    private String name;

    /**
     * calcul(birth_date)>=18
     */
    @Column(name = "birth_date", nullable = false)
    @NotNull(message = "birthDate must be null")
    private LocalDate birthDate;

    /**
     * country
     */
    @NotNull(message = "contry must be null")
    @ManyToOne
    private CountryEntity country;

    /**
     * phone number
     */
    @Column(name = "ohone_number",length = 20)
    private String phoneNumber;

    /**
     * enum
     * gender ={ MALE, FEMALE , OTHER,}
     */
    @Column(name = "gender",length = 20)
    @Enumerated(EnumType.STRING)
    @NotNull(message = "gender is not null")
    private Gender gender;



}
