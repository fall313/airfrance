package com.sig.airFrance.domain;

public enum Gender {
   MALE, FEMALE , OTHER;
}
