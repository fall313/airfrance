package com.sig.airFrance.repository;

import com.sig.airFrance.AirFranceApplication;
import com.sig.airFrance.domain.CountryEntity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test Country repository
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AirFranceApplication.class)
@Transactional
public class CountryRepositoryTest {

    @Autowired
    private CountryRepository repository;

    private CountryEntity country;

    public void createCountry() {

        country = new CountryEntity();
        country.setCode("SN");
        country.setName("Sengal");
        country = repository.save(country);
    }

    @Before
    public void setUp() throws Exception {
    }

    /**
     * Test find all country
     */
    @Test
    public void testFindALL() {
        createCountry();
        List<CountryEntity> result = repository.findAll();
        assertThat(result).isNotNull();
        assertThat(result.size()>=1).isTrue();
    }

}
