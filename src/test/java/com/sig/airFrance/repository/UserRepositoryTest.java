package com.sig.airFrance.repository;

import com.sig.airFrance.AirFranceApplication;
import com.sig.airFrance.domain.CountryEntity;
import com.sig.airFrance.domain.Gender;
import com.sig.airFrance.domain.UserEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;


import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test User repository
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AirFranceApplication.class)
@Transactional
public class UserRepositoryTest {

    @Autowired
    private UserRepository  repository;

    private UserEntity userEntity;

    @Autowired
    private CountryRepository countryRepository;

    public UserEntity creatUser() {

         CountryEntity country= new CountryEntity();
         country.setCode("SN");
        country.setName("Sengal");
        country = countryRepository.save(country);

        userEntity = new UserEntity();
        userEntity.setName("serigne");
        userEntity.setGender(Gender.MALE);
        userEntity.setBirthDate(LocalDate.now());
        userEntity.setCountry(country);
        return repository.save(userEntity);
    }

    @Before
    public void setUp() throws Exception {
    }

    /**
     * Test find all country
     */
    @Test
    public void testFindById() {
        UserEntity user= creatUser();
        UserEntity result = repository.findById(user.getId()).orElse(null);
        assertThat(result).isNotNull();
        assertThat(result.getId()).isNotNull();
    }

}
