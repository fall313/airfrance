package com.sig.airFrance.domaine;

import com.sig.airFrance.domain.CountryEntity;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Country {


        @Test
        public void equalsVerifier() throws Exception {
            CountryEntity countryEntity = new CountryEntity();
            countryEntity.setId(1L);
            CountryEntity countryEntity1 = new CountryEntity();
            countryEntity1.setId(countryEntity.getId());
            assertThat(countryEntity).isEqualTo(countryEntity1);
            countryEntity1.setId(2L);
            assertThat(countryEntity).isNotEqualTo(countryEntity1);
           countryEntity.setId(0L);
            assertThat(countryEntity).isNotEqualTo(countryEntity1);
        }


}
