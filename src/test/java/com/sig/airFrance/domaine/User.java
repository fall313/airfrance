package com.sig.airFrance.domaine;

import com.sig.airFrance.domain.CountryEntity;
import com.sig.airFrance.domain.UserEntity;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class User {


        @Test
        public void equalsVerifier() throws Exception {
            UserEntity ob1 = new UserEntity();
            ob1.setId(1L);
            UserEntity ob2 = new UserEntity();
            ob2.setId(ob1.getId());
            assertThat(ob1).isEqualTo(ob2);
            ob2.setId(2L);
            assertThat(ob1).isNotEqualTo(ob2);
            ob1.setId(0L);
            assertThat(ob1).isNotEqualTo(ob2);
        }


}
