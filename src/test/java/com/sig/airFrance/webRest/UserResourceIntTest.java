package com.sig.airFrance.webRest;


import com.sig.airFrance.AirFranceApplication;

import com.sig.airFrance.domain.Gender;
import com.sig.airFrance.service.ICountryService;
import com.sig.airFrance.service.IUserService;
import com.sig.airFrance.service.dto.CountryDto;
import com.sig.airFrance.service.dto.UserDto;

import com.sig.airFrance.service.utils.Constante;
import lombok.Getter;
import lombok.Setter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.data.web.PageableHandlerMethodArgumentResolver;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;


import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserResourceIntTest REST controller.
 *
 * @see UserResourceIntTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AirFranceApplication.class)
public class UserResourceIntTest {


    @Autowired
    private IUserService userService;

    @Autowired
    private ICountryService countryService;


    private static final String DEFAULT_NUMERO = "773297091";
    private static final String DEFAULT_NAME = "SERIGNE FALL";
    private static final String DEFAULT_EMAIL = "sergine.fall@atos.net";
    private static final String DEFAULT_GENDER = "gender";
    private static final LocalDate DEFAULT_DATE_BIRTH = LocalDate.parse("2000-10-05");


    private static final String DEFAULT_NUMERO2 = "773297091";
    private static final String DEFAULT_NAME2 = "SERIGNE FALL";
    private static final String DEFAULT_EMAIL2 = "sergine.fall@atos.net";
    private static final String DEFAULT_GENDER2 = "gender";
    private static final LocalDate DEFAULT_DATE_BIRTH2 = LocalDate.parse("2000-10-05");


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;


    @Autowired
    private EntityManager em;

    private MockMvc restUserMockMvc;

    private UserDto userDto;

    @Getter
    @Setter
    private CountryDto countryDto = new CountryDto();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserResource userResource = new UserResource(userService);
        this.restUserMockMvc = MockMvcBuilders.standaloneSetup(userResource)
                .setCustomArgumentResolvers(pageableArgumentResolver)
                //  .setControllerAdvice(exceptionTranslator)
                // .setConversionService(createFormattingConversionService())
                .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserDto createEntity(EntityManager em) {

        UserDto dto = new UserDto();
        dto.setName(DEFAULT_NAME);
        dto.setGender(Gender.MALE);
        dto.setBirthDate(DEFAULT_DATE_BIRTH);

        dto.setPhoneNumber(DEFAULT_NUMERO);
        return dto;
    }


    public static UserDto createEntity2(EntityManager em) {

        UserDto dto = new UserDto();
        dto.setName(DEFAULT_NAME2);
        dto.setGender(Gender.MALE);
        dto.setBirthDate(DEFAULT_DATE_BIRTH2);
        dto.setPhoneNumber(DEFAULT_NUMERO2);
        return dto;
    }

    @Before
    public void initTest() {
        countryDto = new CountryDto();
        countryDto.setName("France");
        countryDto.setCode(Constante.CODE_PAYS_FR);
        countryDto = countryService.save(countryDto);

        userDto = createEntity(em);
        userDto.setCountryId(countryDto.getId());

    }

    @Test
    @Transactional
    public void testApiCreate() throws Exception {

        restUserMockMvc.perform(post("/api/users")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDto)))
                .andExpect(status().isCreated());


    }

    @Test
    @Transactional
    public void testApiUpdate() throws Exception {


        UserDto dto = createEntity2(em);
        dto.setCountryId(countryDto.getId());

        restUserMockMvc.perform(put("/api/users")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(dto)))
                .andExpect(status().isBadRequest());


        dto = createEntity2(em);
        dto.setCountryId(countryDto.getId());
        UserDto userDto2 = userService.save(dto);

        restUserMockMvc.perform(put("/api/users")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDto2)))
                .andExpect(status().isOk());


        userDto2.setCountryId(34l);

        restUserMockMvc.perform(put("/api/users")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDto2)))
                .andExpect(status().isNotAcceptable());


    }


    @Test
    @Transactional
    public void checkMineure() throws Exception {
        userDto.setBirthDate(LocalDate.now());

        restUserMockMvc.perform(post("/api/users")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDto)))
                .andExpect(status().isNotAcceptable());

        ;

        userDto.setCountryId(2l);

        restUserMockMvc.perform(post("/api/users")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userDto)))
                .andExpect(status().isNotAcceptable());

        ;

        userDto = userService.getById(1l);

    }


    @Test
    @Transactional
    public void getUser() throws Exception {
        // Initialize the database


        UserDto dto = createEntity2(em);
        dto.setCountryId(countryDto.getId());
        UserDto userDto2 = userService.save(dto);

        restUserMockMvc.perform(get("/api/users/{id}", userDto2.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(userDto2.getId().intValue()))
                .andExpect(jsonPath("$.name").value(DEFAULT_NAME2.toString()))
                .andExpect(jsonPath("$.gender").value(Gender.MALE.toString()))
        ;


        restUserMockMvc.perform(get("/api/users/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound())


        ;

    }


    @Test
    @Transactional
    public void testApiGetAll() throws Exception {
        // Initialize the database


        UserDto dto = createEntity2(em);
        dto.setCountryId(countryDto.getId());
        UserDto userDto2 = userService.save(dto);

        restUserMockMvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*].id").value(hasItem(userDto2.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(userDto2.getName())))
                .andExpect(jsonPath("$.[*].gender").value(hasItem(userDto2.getGender().toString())))


        ;


    }


    @Test
    @Transactional
    public void testApiDelete() throws Exception {
        // Initialize the database


        UserDto dto = createEntity2(em);
        dto.setCountryId(countryDto.getId());
        UserDto userDto2 = userService.save(dto);

        int sizeBefore = userService.getAllUser().size();

        restUserMockMvc.perform(delete("/api/users/{id}", userDto2.getId()))
                .andExpect(status().isOk());

        int sizeAfter = userService.getAllUser().size();
        assertThat(sizeAfter + 1).isEqualTo(sizeBefore);


        restUserMockMvc.perform(delete("/api/users/{id}", Long.MAX_VALUE))
                .andExpect(status().isNoContent());


    }

}
