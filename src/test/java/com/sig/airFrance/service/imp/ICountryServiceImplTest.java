package com.sig.airFrance.service.imp;


import com.sig.airFrance.AirFranceApplication;
import com.sig.airFrance.domain.CountryEntity;
import com.sig.airFrance.repository.CountryRepository;
import com.sig.airFrance.repository.UserRepository;
import com.sig.airFrance.service.ICountryService;
import com.sig.airFrance.service.IUserService;
import com.sig.airFrance.service.dto.CountryDto;
import com.sig.airFrance.service.dto.UserDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test TypeServiceImpl
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AirFranceApplication .class)
@Transactional
public class ICountryServiceImplTest {




    @Autowired
    private ICountryService service;

    @Autowired
    private CountryRepository countryRepository;







    @Test
    public void testSave() {
        CountryDto dto=new CountryDto();
        dto.setName("France");
        dto.setCode("FR");

        dto=service.save(dto);
        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isNotNull();
    }

    @Test
    public void testGetById() {
        CountryDto dto=new CountryDto();
        dto.setName("France");
        dto.setCode("FR");
        dto=service.save(dto);

        dto=new CountryDto();
        dto.setName("Senegal");
        dto.setCode("SN");
        dto=service.save(dto);

        List<CountryEntity> reponse=service.getAll();

        assertThat(reponse).isNotNull();
        assertThat(reponse.size()).isNotNull();
    }



}
