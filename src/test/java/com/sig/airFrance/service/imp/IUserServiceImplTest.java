package com.sig.airFrance.service.imp;


import com.sig.airFrance.AirFranceApplication;
import com.sig.airFrance.domain.Gender;

import com.sig.airFrance.service.ICountryService;
import com.sig.airFrance.service.IUserService;
import com.sig.airFrance.service.dto.CountryDto;
import com.sig.airFrance.service.dto.UserDto;
import com.sig.airFrance.service.utils.Constante;
import com.sig.airFrance.webRest.Utils.ReponseUser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;


import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test TypeServiceImpl
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AirFranceApplication .class)
@Transactional
public class IUserServiceImplTest {


    private static final String DEFAULT_NUMERO = "773297091";
    private static final String DEFAULT_NAME = "SERIGNE FALL";
    private static final String DEFAULT_EMAIL = "sergine.fall@atos.net";
    private static final String DEFAULT_GENDER = "gender";
    private static final LocalDate DEFAULT_DATE_BIRTH = LocalDate.now();

    private static final String DEFAULT_NUMERO_BIS = "773297093";
    private static final String DEFAULT_NAME_BIS = "SERIGNE FALL";
    private static final String DEFAULT_EMAIL_BIS = "sergine.fall@atos.net";
    private static final String DEFAULT_GENDER_BIS = "gender";
    private static final LocalDate DEFAULT_DATE_BIRTH_BIS = LocalDate.now();
    private static final String UPDATE_NAME = "mor diop";

    @Autowired
    private ICountryService countryService;


    @Autowired
    private IUserService service;






    public UserDto creatUser (String name,String gender,String email, String phoneNumber,LocalDate birthDate){


        CountryDto countryDto=new CountryDto();
        countryDto.setName("France");
        countryDto.setCode("Fr");

        countryDto=countryService.save(countryDto);

        UserDto dto = new UserDto();
        dto.setName(name);
        dto.setGender(Gender.MALE);
        dto.setBirthDate(birthDate);
        dto.setPhoneNumber(phoneNumber);
        dto.setCountryId(countryDto.getId());

        return dto;

    }



    @Test
    public void testSave() {
        UserDto dto = creatUser(DEFAULT_NAME,DEFAULT_GENDER,DEFAULT_EMAIL,DEFAULT_NUMERO,DEFAULT_DATE_BIRTH);

        UserDto reponse =service.save(dto);
        assertThat(reponse).isNotNull();


        dto.setBirthDate( LocalDate.parse("2000-10-05"));
        reponse =service.save(dto);
        assertThat(reponse).isNotNull();

    }

    @Test
    public void testUpdate() {
        UserDto dto = creatUser(DEFAULT_NAME,DEFAULT_GENDER,DEFAULT_EMAIL,DEFAULT_NUMERO,DEFAULT_DATE_BIRTH);

        UserDto reponse =service.save(dto);


        reponse.setName(UPDATE_NAME);
        reponse=service.save(reponse);
        assertThat(reponse).isNotNull();
        assertThat(reponse.getName()).isEqualTo(UPDATE_NAME);





    }


    @Test
    public void testDelte() {
        UserDto dto = creatUser(DEFAULT_NAME,DEFAULT_GENDER,DEFAULT_EMAIL,DEFAULT_NUMERO,DEFAULT_DATE_BIRTH);

        UserDto reponse =service.save(dto);

        int sizeBefor=service.getAllUser().size();
        service.deleteUserById(reponse.getId());
        int sizeAfter=service.getAllUser().size();
        assertThat(sizeAfter+1).isEqualTo(sizeBefor);

    }


    @Test
    public void testGetAllUsers() {
        UserDto dto = creatUser(DEFAULT_NAME,DEFAULT_GENDER,DEFAULT_EMAIL,DEFAULT_NUMERO,DEFAULT_DATE_BIRTH);

        UserDto reponse =service.save(dto);

        int sizeListe=service.getAllUser().size();
        assertThat(sizeListe>0).isTrue();

    }





    @Test
    public void testValidation() {
        UserDto dto = creatUser(DEFAULT_NAME,DEFAULT_GENDER,DEFAULT_EMAIL,DEFAULT_NUMERO,DEFAULT_DATE_BIRTH);

        ReponseUser reponse =service.validation(dto);
        assertThat(reponse).isNotNull();
        assertThat(reponse.getStatus()).isEqualTo(Constante.CODE_REPOSE_NOK);
        assertThat(reponse.getUserDto()).isNull();

        dto.setBirthDate( LocalDate.parse("2000-10-05"));
        reponse =service.validation(dto);
        assertThat(reponse).isNotNull();
        assertThat(reponse.getStatus()).isEqualTo(Constante.CODE_REPOSE_OK);


        dto.setCountryId(Long.MAX_VALUE);
        reponse =service.validation(dto);
        assertThat(reponse).isNotNull();
        assertThat(reponse.getStatus()).isEqualTo(Constante.CODE_REPOSE_NOK);
        assertThat(reponse.getUserDto()).isNull();


        dto.setCountryId(null);
        reponse =service.validation(dto);
        assertThat(reponse).isNotNull();
        assertThat(reponse.getStatus()).isEqualTo(Constante.CODE_REPOSE_NOK);
        assertThat(reponse.getUserDto()).isNull();



        CountryDto countryDto=new CountryDto();
        countryDto.setName("Senegal");
        countryDto.setCode("Sn");

        countryDto=countryService.save(countryDto);

        dto.setCountryId(countryDto.getId());
        reponse =service.validation(dto);
        assertThat(reponse).isNotNull();
        assertThat(reponse.getStatus()).isEqualTo(Constante.CODE_REPOSE_NOK);
        assertThat(reponse.getUserDto()).isNull();


    }


    @Test
    public void testGetById() {
        UserDto dto = creatUser(DEFAULT_NAME_BIS,DEFAULT_GENDER_BIS,DEFAULT_EMAIL_BIS,DEFAULT_NUMERO_BIS,DEFAULT_DATE_BIRTH_BIS);
        dto.setBirthDate( LocalDate.parse("2000-10-05"));
       UserDto reponse =service.save(dto);

        UserDto result=service.getById(reponse.getId());

        assertThat(result).isNotNull();
        assertThat(result.getId()).isNotNull();



        result=service.getById(Long.MAX_VALUE);
        assertThat(result).isNull();

    }



}
